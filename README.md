# pyQuASAR

Wrap quasar for pipelines

## Installation

First make sure you have followed the installation procedure for
[pydbsnp](https://github.com/anthony-aylward/pydbsnp). Then install `pyQuASAR`
with `pip3`:

```sh
pip3 install pyQuASAR
```
or
```sh
pip3 install --user pyQuASAR
```

Once pyQuASAR is installed, run `pyQuASAR-download` to download QuASAR and
the SNPs file.

```sh
pyQuASAR-download
```

Now `pyQuASAR` is ready to go!
